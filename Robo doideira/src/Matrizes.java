import lejos.nxt.comm.RConsole;

public class Matrizes {
	public static int[] melhorOpcao(int[][] MapaABS, int[][] MapaREL,int[]pivo, int[][] Posicoes){
		int[][] prob = new int[MapaREL.length][MapaREL[0].length];
		for(int i = 0; i < MapaREL.length; i++){
			for(int j = 0; j < MapaREL[0].length; j++){
				int pontos = 0;
				int[][] arr = {{i+1,j},{i-1,j},{i,j+1},{i,j-1}};	
				if(MapaREL[i][j]==1){									
					for(int lm = 0;lm <4 ; lm++){
						int quantiLivre = 0;
						int quantiObsta = 0;
						for(int k = 0; k<Posicoes.length; k++){
							int[] vetor = new int[2];
							int[] transf = new int[2];
							vetor[0] = arr[lm][0] - pivo[0];
							vetor[1] = arr[lm][1] - pivo[1];
							switch(Posicoes[k][2]){
							case 0:
								transf[0] = vetor[0];
								transf[1] = vetor[1];
								break;
							case 1:
								transf[0] = vetor[1];
								transf[1] = -vetor[0];
								break;
							case 2:
								transf[0] = -vetor[0];
								transf[1] = -vetor[1];
								break;
							case 3:
								transf[0] = -vetor[1];
								transf[1] = vetor[0];
								break;
							}
							int il = Posicoes[k][0]+transf[0];
							int jl = Posicoes[k][1]+transf[1];
							if(il<0||il>=MapaABS.length||jl<0||jl>=MapaABS[0].length){
								quantiObsta++;//considerar as paredes como obstaculos
							}else{
								if(MapaABS[il][jl]==1){
									quantiLivre++;	
								}else{
									quantiObsta++;
								}									
							}
						}
						//RConsole.println("livre: "+quantiLivre);
						pontos += quantiLivre*quantiObsta;
					}
					for(int ln = 0; ln <4; ln++){
						if(arr[ln][0]<0||arr[ln][0]>=MapaREL.length||arr[ln][1]<0||arr[ln][1]>=MapaREL[0].length){
							pontos++;
						}else if(MapaREL[arr[ln][0]][arr[ln][1]]==0){
							pontos++;
						}
					}
				}
				prob[i][j]=pontos;						
			}
		}
		int melhor[] = {0,0};
		for(int i = 0; i<prob.length; i++){
			for(int j = 0; j<prob[0].length; j++){
				if(prob[i][j]>prob[melhor[0]][melhor[1]]){
					melhor[0] = i;
					melhor[1] = j;
				}else if(prob[i][j] == prob[melhor[0]][melhor[1]]){
					double dist_melhor = Math.sqrt(Math.pow(melhor[0]-pivo[0],2)+Math.pow(melhor[1]-pivo[1],2));
					double dist_atual = Math.sqrt(Math.pow(i-pivo[0],2)+Math.pow(j-pivo[1],2));
					if(dist_atual<dist_melhor){
						melhor[0] = i;
						melhor[1] = j;						
					}
				}
			}	
		}
		//Matrizes.imprimeMat(prob);
		return melhor;
	}
	public static int[][] compara_mapa(int[][] MapaCompleto,int[][] MapaIncompleto,int[] p_inicial){
		int[] pivo = {p_inicial[0],p_inicial[1]};
		int[][] saida = {{-1,-1,-1}};
		//----o mapa precisa estar com "paredes" pra a comparacao ficar boa
		int[][] MapaCompletoMaisParede = new int[MapaCompleto.length+2][MapaCompleto[0].length+2];
		for(int i = 0; i<MapaCompleto.length;i++){
			for(int j = 0; j<MapaCompleto[0].length;j++){
				MapaCompletoMaisParede[i+1][j+1] = MapaCompleto[i][j];
			}
		}
		//------Adicionar paredes----------------------------
		for(int i = 0; i<MapaCompletoMaisParede.length;i++){
			MapaCompletoMaisParede[i][0] = 2;
			MapaCompletoMaisParede[i][MapaCompletoMaisParede[0].length-1] = 2;
		}
		for(int i = 0; i<MapaCompletoMaisParede.length;i++){
			MapaCompletoMaisParede[0][i] = 2;
			MapaCompletoMaisParede[MapaCompletoMaisParede.length-1][i] = 2;
		}
		//imprimeMapa(MapaCompletoMaisParede);
		// -----------tirar os zeros da relativa-----------
		for(int w = 0; w<4 ; w++){//fazer para as posicoes possiveis 0� 90� 180� 270�
			if(w!=0){
				int[] auxiliar = {pivo[0], pivo[1]};
				pivo[0] = auxiliar[1];
				pivo[1] = MapaIncompleto.length-1-auxiliar[0]; 
				MapaIncompleto = rotaciona_mapa(MapaIncompleto);
			}
//			RConsole.println("[ "+pivo[0]+" , "+pivo[1]+" ]");
			int iniciol = 0;
			int iniciom = 0;
			for(int l = 0;l<MapaIncompleto.length;l++){
				boolean controle = true;
				for(int m = 0;m<MapaIncompleto[0].length;m++){
					if(MapaIncompleto[l][m]!=0){
						controle = false;
					}
				}
				if(controle){
					iniciol = l+1;
				}else{
					break;
				}
			}
			for(int m = 0;m<MapaIncompleto[0].length;m++){
				boolean controle = true;
				for(int l = 0;l<MapaIncompleto.length;l++){
					if(MapaIncompleto[l][m]!=0){
						controle = false;
					}
				}
				if(controle){
					iniciom = m+1;
				}else{
					break;
				}
			}		
			//------------------------------------------
			//--------comecar a busca-------------------
			for(int i = 0;i<MapaCompletoMaisParede.length;i++){
				for(int j = 0;j<MapaCompletoMaisParede[0].length;j++){
					boolean controle = true;
					for(int l = iniciol;l<MapaIncompleto.length;l++){
						for(int m = iniciom;m<MapaIncompleto[0].length;m++){
							if((i+l)<MapaCompletoMaisParede.length&&(j+m)<MapaCompletoMaisParede[0].length){
								if(MapaIncompleto[l][m]!=0){
									if(MapaIncompleto[l][m]!=MapaCompletoMaisParede[i+l][j+m]){
										controle = false;
										break;
									}
								}
							}else{
								controle = false;
								break;
							}
						}
						if(!controle){
							break;
						}
					}
					if (controle){
						if(saida[0][0]<0){
							saida[0][0] = pivo[0]+i-1; 
							saida[0][1] = pivo[1]+j-1;	
							saida[0][2] = w;
						}else{
							int[][] auxiliar = saida;
							saida = new int[auxiliar.length+1][3];
							for(int cnt = 0; cnt<auxiliar.length;cnt++){
								saida[cnt][0] = auxiliar[cnt][0];
								saida[cnt][1] = auxiliar[cnt][1];
								saida[cnt][2] = auxiliar[cnt][2];
							}
							saida[saida.length-1][0] = pivo[0]+i-1; 
							saida[saida.length-1][1] = pivo[1]+j-1;	
							saida[saida.length-1][2] = w;
						}					 
					}
				}
			}
		}
		pivo[0] = pivo[1];
		pivo[1] = MapaIncompleto.length-1-pivo[0]; 
		MapaIncompleto = rotaciona_mapa(MapaIncompleto);//so pra voltar ele pra pos original
		return saida;
	}
	public static int[] novaCoordenada(int[][] mapa, int[] pos_inicial, int[] pos_final){
		int escolhido[] = {-1,-1};
		for(int dist = 1; dist<mapa.length/2; dist++){
			int [][] lista = new int[8*dist][2];
			for(int k = -dist; k<dist; k++){
				int contador = dist+k;
				lista[contador+0*(lista.length)/4][0] = pos_final[0]+k;
				lista[contador+0*(lista.length)/4][1] = pos_final[1]-dist;
				lista[contador+1*(lista.length)/4][0] = pos_final[0]+dist;
				lista[contador+1*(lista.length)/4][1] = pos_final[1]+k;
				lista[contador+2*(lista.length)/4][0] = pos_final[0]-k;;
				lista[contador+2*(lista.length)/4][1] = pos_final[1]+dist;
				lista[contador+3*(lista.length)/4][0] = pos_final[0]-dist;
				lista[contador+3*(lista.length)/4][1] = pos_final[1]-k;
			}
//			RConsole.print("\n");
//			imprimeMat(lista);
//			RConsole.print("\n");
			boolean controle = false;
			for(int k = 0; k<lista.length; k++){
				if(lista[k][0]>=0&&lista[k][0]<mapa.length&&lista[k][1]>=0&&lista[k][1]<mapa[0].length){
					if(mapa[lista[k][0]][lista[k][1]]==1){
						escolhido[0] = lista[k][0];
						escolhido[1] = lista[k][1];
						controle = true;
					}
				}
			}
			//RConsole.println("esco["+escolhido[0]+","+escolhido[1]+"]");			
			for(int k = 0; k<lista.length; k++){
				if(lista[k][0]>=0&&lista[k][0]<mapa.length&&lista[k][1]>=0&&lista[k][1]<mapa[0].length){
					if(mapa[lista[k][0]][lista[k][1]]==1){
						double distancia = Math.sqrt(Math.pow((pos_final[0]-escolhido[0] ), 2)+Math.pow((pos_final[1]-escolhido[1] ), 2));
						if(distancia>Math.sqrt(Math.pow((pos_final[0]-lista[k][0] ), 2)+Math.pow((pos_final[1]-lista[k][1] ), 2))){
							escolhido[0] = lista[k][0];
							escolhido[1] = lista[k][1];
						}
					}
				}				
			}
			//RConsole.println("esco["+escolhido[0]+","+escolhido[1]+"]");
			if(controle){
				//RConsole.println("break");
				break;
			}
		}
		return escolhido;		
	}
	public static void imprimeMat(int[][] Matriz){
		for(int i = 0;i<Matriz.length;i++){
			RConsole.print("| ");
			for(int j = 0;j<Matriz[0].length;j++){
				RConsole.print(Matriz[i][j]+" ");
			}
			RConsole.print("|");
			RConsole.print("\n");
		}
	}
	public static void ImprimeEstadoRobo(Robo bolingo){
		String tampa = " --";
		RConsole.print("  ");
		for(int j = 0;j<bolingo.mapa_mapeamento[0].length;j++){
			RConsole.print(" "+j);
			tampa += "--";
		}
		tampa += "-";
		RConsole.print("\n"+tampa+"\n");
		for(int i = 0;i<bolingo.mapa_mapeamento.length;i++){			
			RConsole.print(i+"|");
			for(int j = 0;j<bolingo.mapa_mapeamento[0].length;j++){
				if(i==bolingo.pos_robo[0]&&j==bolingo.pos_robo[1]){
					switch(bolingo.orientacao_robo){
					case 0:
						RConsole.print(" >");
						break;
					case 1:
						RConsole.print(" ^");
						break;
					case 2:
						RConsole.print(" <");
						break;
					case 3:
						RConsole.print(" v");
						break;
					}
				}else{
					switch(bolingo.mapa_mapeamento[i][j]){
					case 0:
						RConsole.print(" 0");
						break;
					case 1:
						RConsole.print(" +");
						break;
					case 2:
						RConsole.print(" X");
						break;
					case 3:
						RConsole.print(" ?");
						break;
					}					
				}				
			}
			RConsole.print(" |\n");			
		}
		RConsole.print(tampa+"\n");
	}
	public static void imprimeMapa(int[][] Matriz){
		String tampa = " --";
		RConsole.print("    ");
		for(int j = 0;j<Matriz[0].length;j++){
			RConsole.print(" "+j);
			tampa += "--";
		}
		tampa += "-";
		RConsole.print("\n"+tampa+"\n");
		for(int i = 0;i<Matriz.length;i++){			
			RConsole.print(i+"|");
			for(int j = 0;j<Matriz[0].length;j++){
				switch(Matriz[i][j]){
				case 0:
					RConsole.print(" 0");
					break;
				case 1:
					RConsole.print(" +");
					break;
				case 2:
					RConsole.print(" X");
					break;
				case 3:
					RConsole.print(" ?");
					break;
				case 4:
					RConsole.print(" >");
					break;
				case 5:
					RConsole.print(" ^");
					break;
				case 6:
					RConsole.print(" <");
					break;
				case 7:
					RConsole.print(" v");
					break;
				}				
			}
			RConsole.print(" |\n");			
		}
		RConsole.print(tampa+"\n");
	}
	public static int[][] rotaciona_mapa(int[][] Mapa){
		int[][] saida = new int[Mapa[0].length][Mapa.length];
		for(int i = 0;i<Mapa.length;i++){
			for(int j = 0;j<Mapa[0].length;j++){	
				saida[j][Mapa.length-i-1] = Mapa[i][j];
			}
		}
		return saida;
	}
	public static int[][] preenche_Mapa(int[][] Mapa,int[] posicao, int valor){
		int elem_i = posicao[0];
		int elem_j = posicao[1];

		int[][] saida = new int[Mapa.length][Mapa[0].length];
		for(int i = 0;i<Mapa.length;i++){
			for(int j = 0;j<Mapa[0].length;j++){	
				saida[i][j] = Mapa[i][j];
			}
		}
		if(elem_i<0){
			int[][] auxiliar = saida;
			saida = new int[saida.length+Math.abs(elem_i)][saida[0].length];
			for(int i = 0;i<auxiliar.length;i++){
				for(int j = 0;j<auxiliar[0].length;j++){	
					saida[i+Math.abs(elem_i)][j] = auxiliar[i][j];
				}
			}
			for(int j = 0;j<saida[0].length;j++){
				saida[0][j] = 0;
			}
			elem_i = 0; 
		}
		if(elem_j<0){
			int[][] auxiliar = saida;
			saida = new int[saida.length][saida[0].length+Math.abs(elem_j)];
			for(int i = 0;i<auxiliar.length;i++){
				for(int j = 0;j<auxiliar[0].length;j++){	
					saida[i][j+Math.abs(elem_j)] = auxiliar[i][j];
				}
			}
			for(int i = 0;i<saida.length;i++){
				saida[i][0] = 0;
			}
			elem_j = 0; 
		}
		if(elem_i>=saida.length){
			int[][] auxiliar = saida;
			saida = new int[elem_i+1][saida[0].length];
			for(int i = 0;i<auxiliar.length;i++){
				for(int j = 0;j<auxiliar[0].length;j++){	
					saida[i][j] = auxiliar[i][j];
				}
			}
			for(int j = 0;j<saida[0].length;j++){
				saida[saida.length-1][j] = 0;
			}
		}
		if(elem_j>=saida[0].length){
			int[][] auxiliar = saida;
			saida = new int[saida.length][elem_j+1];
			for(int i = 0;i<auxiliar.length;i++){
				for(int j = 0;j<auxiliar[0].length;j++){	
					saida[i][j] = auxiliar[i][j];
				}
			}
			for(int i = 0;i<saida.length;i++){
				saida[i][saida[0].length-1] = 0;
			}
		}
		saida[elem_i][elem_j] = valor;
		return saida;
	}
}
