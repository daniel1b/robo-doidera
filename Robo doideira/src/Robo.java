

public class Robo {
	public int[][] mapa_mapeamento;
	public int orientacao_robo;
	public int[] pos_robo;
	public Robo(int[][] mapa_mapeamento,int orientacao_robo,int[] pos_robo){
		this.mapa_mapeamento = mapa_mapeamento;
		this.orientacao_robo = orientacao_robo;
		this.pos_robo = pos_robo;
	}
	public void giraD(){
		//orientacao 0 => 0�
		//orientacao 1 => 90�
		//orientacao 2 => 180�
		//orientacao 3 => 270�
		orientacao_robo = orientacao_robo - 1;
		if(orientacao_robo<0){
			orientacao_robo = 3;
		}
	}
	public int[] get1PosicaoFrente(){
		
		int [] saida = new int[2];
		saida[0] = pos_robo[0];
		saida[1] = pos_robo[1];
		if(orientacao_robo==0){
			saida[1] = saida[1]+1;
		}else if(orientacao_robo==1){
			saida[0] = saida[0]-1;
		}if(orientacao_robo==2){
			saida[1] = saida[1]-1;
		}if(orientacao_robo==3){
			saida[0] = saida[0]+1;
		}
		return saida;
	}
	public int[] get2PosicaoFrente(){
		int [] saida = pos_robo;
		if(orientacao_robo==0){
			saida[1] = saida[1]+2;
		}else if(orientacao_robo==1){
			saida[0] = saida[0]-2;
		}if(orientacao_robo==2){
			saida[1] = saida[1]-2;
		}if(orientacao_robo==3){
			saida[0] = saida[0]+2;
		}
		return saida;
	}
	public void giraE(){
		//orientacao 0 => 0�
		//orientacao 1 => 90�
		//orientacao 2 => 180�
		//orientacao 3 => 270�
		orientacao_robo = orientacao_robo + 1;
		if(orientacao_robo>3){
			orientacao_robo = 0;
		}
	}
	public void avancaCelula(){//avanca um posicao de acordo com a orientacao do robo
		if(orientacao_robo==0){
			pos_robo[1] = pos_robo[1]+1;
		}else if(orientacao_robo==1){
			pos_robo[0] = pos_robo[0]-1;
		}if(orientacao_robo==2){
			pos_robo[1] = pos_robo[1]-1;
		}if(orientacao_robo==3){
			pos_robo[0] = pos_robo[0]+1;
		}
	}
	//==================================================================================================
	public int[] busca_celula_cinza(int i){
		int[] retorno = {-1,-1};//retornar negativo quando nao houver celulas desconhecidas
		int colunas = mapa_mapeamento[0].length;
		
		int menorj = -1;
		for(int j = 0; j<colunas;j++){//pegar a celula cinza de menor j
			if(mapa_mapeamento[i][j]==0){
				menorj = j;
				break;
			}				
		}
		
		int maiorj = -1;
		for(int j = colunas-1; j>=0 ; j--){//pegar a celula cinza de maior j
			if(mapa_mapeamento[i][j]==0){
				maiorj = j;
				break;
			}				
		}
		
		if(maiorj>=0){//se houver alguma celula cinza
			//descobrir qual delas esta mais proxima do robo
			if(Math.abs(maiorj-pos_robo[1])<Math.abs(menorj-pos_robo[1])){
				retorno[0] = i;
				retorno[1] = maiorj;
			}else{
				retorno[0] = i;
				retorno[1] = menorj;
			}
		}
		//retornar a celula cinza mais proxima
		//ou retornar negativo pra indicar que nao tem celula cinza
		return retorno;
	}
}
