import java.util.ArrayList;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.comm.RConsole;
import lejos.util.Delay;

public class Trajetoria {
	public static void main(String[] args) {
		//		Button.waitForAnyPress();
		//		while(true){
		//			if(Leitura.valorSensorUltraSon()){
		//				LCD.drawString("    Tem obstaculo", 0, 7);
		//			}else{
		//				LCD.drawString("nao Tem obstaculo", 0, 7);
		//			};
		//			
		//		}
		//--------------Conexao via bluetooth--------------------------------------
		RConsole.openBluetooth(30000);
		//--------------Iniciar a calibra��o dos dois sensores---------------------
		LCD.clear();
		LCD.drawString("Calibrar branco", 0, 7);
		Button.waitForAnyPress();
		int[] branco = Leitura.Calibracao();

		LCD.clear();
		LCD.drawString("Calibrar preto", 0, 7);
		Button.waitForAnyPress();
		int[] preto = Leitura.Calibracao();

		int limite[]=new int[2];
		for (int i = 0;i < 2;i++)
		{
			limite[i] =( preto[i] + 2*branco[i] )/3;
		}

		LCD.clear();
		LCD.drawString("limite E "+limite[0], 0, 1);
		LCD.drawString("limite D "+limite[1], 0, 2);
		//------------------------Fim da calibra��o--------------------------------		
		//-------------------------Inicio Mapaemento-------------------------------
		LCD.drawString("Comecar mapeamento", 0, 7);
		Button.waitForAnyPress();
		LCD.clear();
		//matriz pra fazer o mapeamento
		//int [][]mapa_mapeamento = new int[10][10];
		int [][]mapa_mapeamento = {
				//   0  1  2  3  4  5  6  7  8  9
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, //0
				{1, 2, 1, 1, 1, 1, 1, 2, 1, 2}, //1
				{1, 1, 2, 1, 1, 1, 1, 2, 2, 1}, //2
				{1, 1, 1, 2, 1, 1, 2, 1, 1, 1}, //3
				{1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, //4
				{1, 1, 2, 1, 2, 1, 1, 1, 1, 1}, //5
				{1, 2, 1, 2, 1, 2, 1, 1, 2, 1}, //6
				{1, 1, 2, 1, 1, 1, 1, 2, 1, 1}, //7
				{1, 2, 1, 1, 1, 1, 1, 1, 1, 1}, //8
				{2, 1, 1, 1, 1, 1, 1, 1, 1, 1}, //9

		};
		//0 - desconhecido			2 - oucupado
		//1 - livre					3 - inatingivel
		for(int i = 0; i<mapa_mapeamento.length; i++){
			for(int j = 0; j<mapa_mapeamento[0].length; j++){
				mapa_mapeamento[i][j] = 0; //dizer que as celulas sao desocnhecidas
			}
		}
		//posicao inicial do robo
		//comecar em baixo pra ficar mais parecida com o campo
		int[] pos_robo = {9,0}; //linha 9 e coluna 0
		int   orientacao_robo = 0; 
		//orientacao 0 => 0�
		//orientacao 1 => 90�
		//orientacao 2 => 180�
		//orientacao 3 => 270�

		mapa_mapeamento[pos_robo[0]][pos_robo[1]] = 1;

		Robo bolingo = new Robo(mapa_mapeamento, orientacao_robo, pos_robo);

		int celulas_sem_mudar_direcao = 0;

		for(int i = 9; i>=0; i--){//comecar na posicao inicial e ir varrendo de linha em linha
			Matrizes.ImprimeEstadoRobo(bolingo);
			while(true){//unico modo de sair desse � pelo break				
				int [] proxima_celula = bolingo.busca_celula_cinza(i);				
				if(proxima_celula[0]<0){//se nao houver mais celulas cinza ir p/ prox linha
					break;
				}else{
					//while posicao do robo != proxima celula
					while(!(bolingo.pos_robo[0]==proxima_celula[0]&&bolingo.pos_robo[1]==proxima_celula[1])){						
						if(bolingo.mapa_mapeamento[proxima_celula[0]][proxima_celula[1]] == 2 || bolingo.mapa_mapeamento[proxima_celula[0]][proxima_celula[1]] != 0){
							System.out.println("buscar novo destino ...");
							break;
						}
						//verificar se a celula � atingivel, colocar uma funcao contaminante nela
						v_caminho verf = new v_caminho(bolingo.mapa_mapeamento, proxima_celula, bolingo.pos_robo);

						if(!verf.start()){//dar break se a celula nao � atingivel	
							RConsole.println("nao atingivel");
							bolingo.mapa_mapeamento[proxima_celula[0]][proxima_celula[1]] = 3;//marcar que nao � atingivel
							break;
						}

						//buscar um caminho ente a pos_robo e a proxima celula, mesmo passando por cinza
						try{
							Astar A_Estrela = new Astar(bolingo.mapa_mapeamento);
							Posicao atual = new Posicao(bolingo.pos_robo);
							Posicao objetivo = new Posicao(proxima_celula);
							RConsole.print("posicao -> destino:\n[ "+bolingo.pos_robo[0]+" , "+bolingo.pos_robo[1]+" ] ->");
							RConsole.print("[ "+proxima_celula[0]+" , "+proxima_celula[1]+" ]\n");
							ArrayList<Integer[]> caminho = A_Estrela.search(atual, objetivo);
							RConsole.println("Caminho:");
							for (int kji = 0; kji <caminho.size(); kji++) {
								RConsole.print("[ "+caminho.get(kji)[0]+" , "+caminho.get(kji)[1]+" ] ->");
							}
							RConsole.println("\n");
							for (int kji = 0; kji <caminho.size(); kji++) {
								int orientacao_desejada = 0;
								if((bolingo.pos_robo[0]==caminho.get(kji)[0])&&(caminho.get(kji)[1]>bolingo.pos_robo[1])){
									orientacao_desejada = 0;
								}else if((bolingo.pos_robo[0]==caminho.get(kji)[0])&&(caminho.get(kji)[1]<bolingo.pos_robo[1])){
									orientacao_desejada = 2;
								}else if((bolingo.pos_robo[1]==caminho.get(kji)[1])&&(caminho.get(kji)[0]>bolingo.pos_robo[0])){
									orientacao_desejada = 3;
								}else if((bolingo.pos_robo[1]==caminho.get(kji)[1])&&(caminho.get(kji)[0]<bolingo.pos_robo[0])){
									orientacao_desejada = 1;
								}
								orientacao_desejada --;
								if(orientacao_desejada<0){
									orientacao_desejada += 3;
								}
								int p_i = bolingo.pos_robo[0];
								int p_j = bolingo.pos_robo[1];
								int[][] olhar_em = new int[4][2];
								//sequencia mais eficiente, sempre no sentido horario
								switch(orientacao_desejada){
								case 0:
									olhar_em[0][0] = p_i;   olhar_em[0][1] = p_j+1; 
									olhar_em[1][0] = p_i+1; olhar_em[1][1] = p_j; 
									olhar_em[2][0] = p_i;   olhar_em[2][1] = p_j-1; 
									olhar_em[3][0] = p_i-1; olhar_em[3][1] = p_j; 
									break;
								case 1:
									olhar_em[1][0] = p_i;   olhar_em[1][1] = p_j+1; 
									olhar_em[2][0] = p_i+1; olhar_em[2][1] = p_j; 
									olhar_em[3][0] = p_i;   olhar_em[3][1] = p_j-1; 
									olhar_em[0][0] = p_i-1; olhar_em[0][1] = p_j; 
									break;
								case 2:
									olhar_em[2][0] = p_i;   olhar_em[2][1] = p_j+1; 
									olhar_em[3][0] = p_i+1; olhar_em[3][1] = p_j; 
									olhar_em[0][0] = p_i;   olhar_em[0][1] = p_j-1; 
									olhar_em[1][0] = p_i-1; olhar_em[1][1] = p_j;  
									break;
								case 3:
									olhar_em[3][0] = p_i;   olhar_em[3][1] = p_j+1; 
									olhar_em[2][0] = p_i+1; olhar_em[2][1] = p_j; 
									olhar_em[1][0] = p_i;   olhar_em[1][1] = p_j-1; 
									olhar_em[0][0] = p_i-1; olhar_em[0][1] = p_j; 
									break;
								}
								int[][] desconhe = new int[4][2];
								int q_desconhe = 0;
								for(int mi = 0; mi<4; mi++){
									if(olhar_em[mi][0]<0||olhar_em[mi][1]<0||
											olhar_em[mi][0]>=bolingo.mapa_mapeamento.length||
											olhar_em[mi][1]>=bolingo.mapa_mapeamento[0].length){
										RConsole.print("\n fora dos limites");
									}else if(bolingo.mapa_mapeamento[olhar_em[mi][0]][olhar_em[mi][1]]==0){
										/// esta celula � desconhecida	
										desconhe[q_desconhe][0] = olhar_em[mi][0];
										desconhe[q_desconhe][1] = olhar_em[mi][1];
										q_desconhe++;
									}
								}
								//agora que ja se sabe quais sao as que eu nao conheco
								for(int mi = 0; mi<q_desconhe; mi++){
									//descobrir pra que lado girar
									orientacao_desejada = 0;
									if((bolingo.pos_robo[0]==desconhe[mi][0])&&(desconhe[mi][1]>bolingo.pos_robo[1])){
										orientacao_desejada = 0;
									}else if((bolingo.pos_robo[0]==desconhe[mi][0])&&(desconhe[mi][1]<bolingo.pos_robo[1])){
										orientacao_desejada = 2;
									}else if((bolingo.pos_robo[1]==desconhe[mi][1])&&(desconhe[mi][0]>bolingo.pos_robo[0])){
										orientacao_desejada = 3;
									}else if((bolingo.pos_robo[1]==desconhe[mi][1])&&(desconhe[mi][0]<bolingo.pos_robo[0])){
										orientacao_desejada = 1;
									}
									int contador = 0;
									int orientacao_r = bolingo.orientacao_robo;
									while(orientacao_r!=orientacao_desejada){
										orientacao_r++;
										contador++;
										if(orientacao_r>3){
											orientacao_r = 0;
										}
									}
									switch(contador){
									case 3:
										Navegation.virar90Direita();
										bolingo.giraD();
										celulas_sem_mudar_direcao = 0;
										break;
									case 2:
										Navegation.virar90Esquerda();
										Navegation.virar90Esquerda();
										bolingo.giraD();
										bolingo.giraD();
										celulas_sem_mudar_direcao = 0;
										break;
									case 1:
										Navegation.virar90Esquerda();									
										bolingo.giraE();
										celulas_sem_mudar_direcao = 0;
										break;
									case 0:		
										celulas_sem_mudar_direcao++;
										break;
									}
									//ler celula a frente
									int[] CelulaDaFrente  = bolingo.get1PosicaoFrente();	
									//RConsole.println("[ "+CelulaDaFrente[0]+" , "+CelulaDaFrente[1]+" ] = ?\n");
									//int n = ler.nextInt();		
									if(bolingo.mapa_mapeamento[CelulaDaFrente[0]][CelulaDaFrente[1]] == 0){
										Navegation.centralizarNaCelula2(limite);
										if(Leitura.obstaculo()){
											bolingo.mapa_mapeamento[CelulaDaFrente[0]][CelulaDaFrente[1]] = 2;
										}else{
											bolingo.mapa_mapeamento[CelulaDaFrente[0]][CelulaDaFrente[1]] = 1;
										}
									}
								}
								if(caminho.get(caminho.size()-1)[1]!=0&&caminho.get(caminho.size()-1)[1]!=bolingo.mapa_mapeamento[0].length&&bolingo.mapa_mapeamento[caminho.get(caminho.size()-1)[0]][caminho.get(caminho.size()-1)[1]]!=0){
									break;
								}
								if(bolingo.mapa_mapeamento[caminho.get(kji)[0]][caminho.get(kji)[1]]==2){
									break;
								}
								orientacao_desejada = 0;
								if((bolingo.pos_robo[0]==caminho.get(kji)[0])&&(caminho.get(kji)[1]>bolingo.pos_robo[1])){
									orientacao_desejada = 0;
								}else if((bolingo.pos_robo[0]==caminho.get(kji)[0])&&(caminho.get(kji)[1]<bolingo.pos_robo[1])){
									orientacao_desejada = 2;
								}else if((bolingo.pos_robo[1]==caminho.get(kji)[1])&&(caminho.get(kji)[0]>bolingo.pos_robo[0])){
									orientacao_desejada = 3;
								}else if((bolingo.pos_robo[1]==caminho.get(kji)[1])&&(caminho.get(kji)[0]<bolingo.pos_robo[0])){
									orientacao_desejada = 1;
								}
								int contador = 0;
								int orientacao_r = bolingo.orientacao_robo;
								while(orientacao_r!=orientacao_desejada){
									orientacao_r++;
									contador++;
									if(orientacao_r>3){
										orientacao_r = 0;
									}
								}
								switch(contador){
								case 3:
									Navegation.virar90Direita();
									celulas_sem_mudar_direcao = 0;
									bolingo.giraD();
									break;
								case 2:
									Navegation.virar90Esquerda();
									Navegation.virar90Esquerda();
									bolingo.giraD();
									bolingo.giraD();
									celulas_sem_mudar_direcao = 0;
									break;
								case 1:
									Navegation.virar90Esquerda();									
									bolingo.giraE();
									celulas_sem_mudar_direcao = 0;
									break;
								case 0:	
									celulas_sem_mudar_direcao ++;
									break;
								}	
								//ler celula
								//Matrizes.ImprimeEstadoRobo(bolingo);

								int[] CelulaDaFrente  = bolingo.get1PosicaoFrente();	
								//								//RConsole.println("[ "+CelulaDaFrente[0]+" , "+CelulaDaFrente[1]+" ] = ?\n");
								//								//int n = ler.nextInt();		
								//								if(bolingo.mapa_mapeamento[CelulaDaFrente[0]][CelulaDaFrente[1]] == 0){
								//									Navegation.centralizarNaCelula2(limite);
								//									if(Leitura.obstaculo()){
								//										bolingo.mapa_mapeamento[CelulaDaFrente[0]][CelulaDaFrente[1]] = 2;
								//									}else{
								//										bolingo.mapa_mapeamento[CelulaDaFrente[0]][CelulaDaFrente[1]] = 1;
								//									}
								//								}
								//
								//								Matrizes.ImprimeEstadoRobo(bolingo);
								/*================================================
								 * ================================================
								 * ================================================
								 * ================================================
								 * */

								if(caminho.get(caminho.size()-1)[1]!=0&&caminho.get(caminho.size()-1)[1]!=bolingo.mapa_mapeamento[0].length&&bolingo.mapa_mapeamento[caminho.get(caminho.size()-1)[0]][caminho.get(caminho.size()-1)[1]]!=0){
									break;
								}
								if(bolingo.mapa_mapeamento[CelulaDaFrente[0]][CelulaDaFrente[1]]==2){
									break;
								}else{
									if(celulas_sem_mudar_direcao>2){
										celulas_sem_mudar_direcao = 0;
										Navegation.centralizarNaCelula(limite);
									}
									bolingo.avancaCelula();	
									Navegation.avancarCelula(limite);
								}

							}							
						}catch(Exception e){
							e.printStackTrace();
						}
						//RConsole.println("[ "+bolingo.pos_robo[0]+" , "+bolingo.pos_robo[1]+" ]");
						//RConsole.println("[ "+bolingo.pos_robo[0]+" , "+bolingo.pos_robo[1]+" ]");

					}
				}
			}
		}		
		Matrizes.ImprimeEstadoRobo(bolingo);
		//----------------------------Fim mapaemento-------------------------------
		//---------------------------inicio de navega��o---------------------------
		celulas_sem_mudar_direcao = 0;
		Delay.msDelay(1000);
		int posicao_i = bolingo.pos_robo[0];
		int posicao_j = bolingo.pos_robo[1];
		int orientacao_esc = bolingo.orientacao_robo;
		int controle = 0;
		LCD.clear();
		while(controle<3){			
			LCD.drawString("Posicao Inicial", 0, 3);
			LCD.drawString("[ "+posicao_i+" , "+posicao_j+" ] "+orientacao_esc, 0, 4);
			if(controle==0){
				LCD.drawString("  -         ", 0, 5);
			}else if(controle==1){
				LCD.drawString("     -     ", 0, 5);
			}else{
				LCD.drawString("          -", 0, 5);
			}
			if(Button.RIGHT.isDown()){
				if(controle==0){
					posicao_i++;
				}else if(controle==1){
					posicao_j++;
				}else{
					orientacao_esc++;
				}

				Delay.msDelay(500);
			}
			if(Button.LEFT.isDown()){
				if(controle==0){
					posicao_i--;
				}else if(controle==1){
					posicao_j--;
				}else{
					orientacao_esc--;
				}
				Delay.msDelay(500);
			}
			if(Button.ENTER.isDown()){
				controle++;
				Delay.msDelay(500);
			}			
		}
		bolingo.pos_robo[0] = posicao_i;
		bolingo.pos_robo[1] = posicao_j;
		bolingo.orientacao_robo = orientacao_esc;
		Matrizes.ImprimeEstadoRobo(bolingo);
		//-----------------------destino------------------------------------------
		Delay.msDelay(1000);
		posicao_i = bolingo.pos_robo[0];//default
		posicao_j = bolingo.pos_robo[1];//default
		controle=0;
		LCD.clear();

		while(controle<2){			
			LCD.drawString("Posicao Final", 0, 3);
			LCD.drawString("[ "+posicao_i+" , "+posicao_j+" ] ", 0, 4);
			if(controle==0){
				LCD.drawString("  -         ", 0, 5);
			}else {
				LCD.drawString("     -     ", 0, 5);
			}
			if(Button.RIGHT.isDown()){
				if(controle==0){
					posicao_i++;
				}else{
					posicao_j++;
				}

				Delay.msDelay(500);
			}
			if(Button.LEFT.isDown()){
				if(controle==0){
					posicao_i--;
				}else {
					posicao_j--;
				}
				Delay.msDelay(500);
			}
			if(Button.ENTER.isDown()){
				controle++;
				Delay.msDelay(500);
			}			
		}
		LCD.clear();
		LCD.drawString("["+bolingo.pos_robo[0]+","+bolingo.pos_robo[1]+"]->["+posicao_i+","+posicao_j+"]", 0, 3);
		LCD.drawString("Aperte para iniciar", 0, 4);
		Button.waitForAnyPress();
		LCD.clear();
		int[] novasCoord = new int[]{posicao_i,posicao_j};		
		try{
			if (bolingo.mapa_mapeamento[posicao_i][posicao_j]!=1){
				novasCoord = Matrizes.novaCoordenada(bolingo.mapa_mapeamento,bolingo.pos_robo,new int[]{posicao_i,posicao_j});
				LCD.drawString("Nao atingivel", 0, 4);
				LCD.drawString("ir p/["+novasCoord[0]+","+novasCoord[1]+"]", 0, 5);
				Button.waitForAnyPress();
				Button.waitForAnyPress();
			}
			Astar A_Estrela = new Astar(bolingo.mapa_mapeamento);
			Posicao atual = new Posicao(bolingo.pos_robo);
			Posicao objetivo = new Posicao(novasCoord);
			RConsole.print("posicao -> destino:\n[ "+bolingo.pos_robo[0]+" , "+bolingo.pos_robo[1]+" ] ->");
			RConsole.print("[ "+objetivo.x+" , "+objetivo.y+" ]\n");
			ArrayList<Integer[]> caminho = A_Estrela.search(atual, objetivo);
			RConsole.println("Caminho de "+caminho.size()+" cel:");
			for (int kji = 0; kji <caminho.size(); kji++) {
				RConsole.print("[ "+caminho.get(kji)[0]+" , "+caminho.get(kji)[1]+" ] ->");
			}
			RConsole.println("\n");
			for (int kji = 0; kji <caminho.size(); kji++) {
				int orientacao_desejada = 0;
				if((bolingo.pos_robo[0]==caminho.get(kji)[0])&&(caminho.get(kji)[1]>bolingo.pos_robo[1])){
					orientacao_desejada = 0;
				}else if((bolingo.pos_robo[0]==caminho.get(kji)[0])&&(caminho.get(kji)[1]<bolingo.pos_robo[1])){
					orientacao_desejada = 2;
				}else if((bolingo.pos_robo[1]==caminho.get(kji)[1])&&(caminho.get(kji)[0]>bolingo.pos_robo[0])){
					orientacao_desejada = 3;
				}else if((bolingo.pos_robo[1]==caminho.get(kji)[1])&&(caminho.get(kji)[0]<bolingo.pos_robo[0])){
					orientacao_desejada = 1;
				}
				int contador = 0;
				int orientacao_r = bolingo.orientacao_robo;
				while(orientacao_r!=orientacao_desejada){
					orientacao_r++;
					contador++;
					if(orientacao_r>3){
						orientacao_r = 0;
					}
				}
				switch(contador){
				case 3:
					bolingo.giraD();
					Navegation.virar90Direita();
					celulas_sem_mudar_direcao = 0;
					break;
				case 2:
					bolingo.giraD();
					bolingo.giraD();
					Navegation.virar90Direita();
					Navegation.virar90Direita();
					celulas_sem_mudar_direcao = 0;
					break;
				case 1:
					bolingo.giraE();
					Navegation.virar90Esquerda();
					celulas_sem_mudar_direcao = 0;
					break;
				case 0:	
					celulas_sem_mudar_direcao++;
					break;
				}		
				//ler celula
				//Matrizes.ImprimeEstadoRobo(bolingo);		
				bolingo.avancaCelula();
				if(celulas_sem_mudar_direcao>3){
					celulas_sem_mudar_direcao = 0;
					Navegation.centralizarNaCelula(limite);
				}
				Navegation.avancarCelula(limite);

				//RConsole.println("[ "+bolingo.pos_robo[0]+" , "+bolingo.pos_robo[1]+" ]");
			}	
			Matrizes.ImprimeEstadoRobo(bolingo);
		}catch(Exception e){
			e.printStackTrace();
		}		
		//----------------------------Fim navegacao-------------------------------
		//---------------------------inicio sequestro------------------------------
		LCD.clear();
		LCD.drawString("Espera sequestro", 0, 2);
		LCD.drawString("     0 0        ", 0, 3);
		LCD.drawString("     ___        ", 0, 4);
		Button.waitForAnyPress();
		LCD.clear();

		int[][] mapa_sequestrado = {{1}};
		int orientacao_sequestrado = 0;
		int[] pos_sequestrado = {0,0};
		Robo bolingo_sequestrado = new Robo(mapa_sequestrado, orientacao_sequestrado, pos_sequestrado);
		int[] CelulaDaFrente_seq = bolingo_sequestrado.get1PosicaoFrente();


		boolean terminar = false;
		int[][] posicoes = {{-1,-1}};
		int movimentos = -1;
		do{
			movimentos++;
			///fazer o reconhecimento das celulas visinhas
			int p_i = bolingo_sequestrado.pos_robo[0];
			int p_j = bolingo_sequestrado.pos_robo[1];
			int[][] olhar_em = new int[4][2];
			//sequencia mais eficiente, sempre no sentido horario
			switch(bolingo_sequestrado.orientacao_robo){
			case 0:
				olhar_em[0][0] = p_i;   olhar_em[0][1] = p_j+1; 
				olhar_em[1][0] = p_i+1; olhar_em[1][1] = p_j; 
				olhar_em[2][0] = p_i;   olhar_em[2][1] = p_j-1; 
				olhar_em[3][0] = p_i-1; olhar_em[3][1] = p_j; 
				break;
			case 1:
				olhar_em[1][0] = p_i;   olhar_em[1][1] = p_j+1; 
				olhar_em[2][0] = p_i+1; olhar_em[2][1] = p_j; 
				olhar_em[3][0] = p_i;   olhar_em[3][1] = p_j-1; 
				olhar_em[0][0] = p_i-1; olhar_em[0][1] = p_j; 
				break;
			case 2:
				olhar_em[2][0] = p_i;   olhar_em[2][1] = p_j+1; 
				olhar_em[3][0] = p_i+1; olhar_em[3][1] = p_j; 
				olhar_em[0][0] = p_i;   olhar_em[0][1] = p_j-1; 
				olhar_em[1][0] = p_i-1; olhar_em[1][1] = p_j;  
				break;
			case 3:
				olhar_em[3][0] = p_i;   olhar_em[3][1] = p_j+1; 
				olhar_em[2][0] = p_i+1; olhar_em[2][1] = p_j; 
				olhar_em[1][0] = p_i;   olhar_em[1][1] = p_j-1; 
				olhar_em[0][0] = p_i-1; olhar_em[0][1] = p_j; 
				break;
			}
			int[][] desconhe = new int[4][2];
			int q_desconhe = 0;
			for(int i = 0; i<4; i++){
				if(olhar_em[i][0]<0||olhar_em[i][1]<0||
						olhar_em[i][0]>=bolingo_sequestrado.mapa_mapeamento.length||
						olhar_em[i][1]>=bolingo_sequestrado.mapa_mapeamento[0].length){
					/// esta celula � desconhecida
					desconhe[q_desconhe][0] = olhar_em[i][0];
					desconhe[q_desconhe][1] = olhar_em[i][1];
					q_desconhe++;
				}else if(bolingo_sequestrado.mapa_mapeamento[olhar_em[i][0]][olhar_em[i][1]]==0){
					/// esta celula � desconhecida	
					desconhe[q_desconhe][0] = olhar_em[i][0];
					desconhe[q_desconhe][1] = olhar_em[i][1];
					q_desconhe++;
				}
			}
			//agora que ja se sabe quais sao as que eu nao conheco
			for(int i = 0; i<q_desconhe; i++){
				//descobrir pra que lado girar
				int orientacao_desejada = 0;
				if((bolingo_sequestrado.pos_robo[0]==desconhe[i][0])&&(desconhe[i][1]>bolingo_sequestrado.pos_robo[1])){
					orientacao_desejada = 0;
				}else if((bolingo_sequestrado.pos_robo[0]==desconhe[i][0])&&(desconhe[i][1]<bolingo_sequestrado.pos_robo[1])){
					orientacao_desejada = 2;
				}else if((bolingo_sequestrado.pos_robo[1]==desconhe[i][1])&&(desconhe[i][0]>bolingo_sequestrado.pos_robo[0])){
					orientacao_desejada = 3;
				}else if((bolingo_sequestrado.pos_robo[1]==desconhe[i][1])&&(desconhe[i][0]<bolingo_sequestrado.pos_robo[0])){
					orientacao_desejada = 1;
				}
				int contador = 0;
				int orientacao_r = bolingo_sequestrado.orientacao_robo;
				while(orientacao_r!=orientacao_desejada){
					orientacao_r++;
					contador++;
					if(orientacao_r>3){
						orientacao_r = 0;
					}
				}
				switch(contador){
				case 3:
					Navegation.virar90Direita();
					bolingo_sequestrado.giraD();
					break;
				case 2:
					Navegation.virar90Esquerda();
					Navegation.virar90Esquerda();
					bolingo_sequestrado.giraD();
					bolingo_sequestrado.giraD();
					break;
				case 1:
					Navegation.virar90Esquerda();									
					bolingo_sequestrado.giraE();
					break;
				case 0:		
					break;
				}
				//ler celula a frente
				CelulaDaFrente_seq  = bolingo_sequestrado.get1PosicaoFrente();
				Navegation.centralizarNaCelula2(limite);
				if(CelulaDaFrente_seq[0]<0||CelulaDaFrente_seq[1]<0||
						CelulaDaFrente_seq[0]>=bolingo_sequestrado.mapa_mapeamento.length||CelulaDaFrente_seq[1]>=bolingo_sequestrado.mapa_mapeamento[0].length){
					if(Leitura.obstaculo()){
						bolingo_sequestrado.mapa_mapeamento = Matrizes.preenche_Mapa(bolingo_sequestrado.mapa_mapeamento, CelulaDaFrente_seq, 2);
					}else{
						bolingo_sequestrado.mapa_mapeamento = Matrizes.preenche_Mapa(bolingo_sequestrado.mapa_mapeamento, CelulaDaFrente_seq, 1);
					}
				}else if (bolingo_sequestrado.mapa_mapeamento[CelulaDaFrente_seq[0]][CelulaDaFrente_seq[1]]==0){
					if(Leitura.obstaculo()){
						bolingo_sequestrado.mapa_mapeamento = Matrizes.preenche_Mapa(bolingo_sequestrado.mapa_mapeamento, CelulaDaFrente_seq, 2);
					}else{
						bolingo_sequestrado.mapa_mapeamento = Matrizes.preenche_Mapa(bolingo_sequestrado.mapa_mapeamento, CelulaDaFrente_seq, 1);
					}
				}

				if(CelulaDaFrente_seq[0]<0){
					bolingo_sequestrado.pos_robo[0] = bolingo_sequestrado.pos_robo[0]+Math.abs(CelulaDaFrente_seq[0]);
					for(int j = 0; j<4; j++){
						desconhe[j][0] = desconhe[j][0]+Math.abs(CelulaDaFrente_seq[0]);;
					}
				}
				if(CelulaDaFrente_seq[1]<0){
					bolingo_sequestrado.pos_robo[1] = bolingo_sequestrado.pos_robo[1]+Math.abs(CelulaDaFrente_seq[1]);
					for(int j = 0; j<4; j++){
						desconhe[j][1] = desconhe[j][1]+Math.abs(CelulaDaFrente_seq[1]);;
					}
				}
			}
			Matrizes.ImprimeEstadoRobo(bolingo_sequestrado);				
			//verificar se sabe onde esta, senao
			int[][] possiveis_posicoes = Matrizes.compara_mapa(bolingo.mapa_mapeamento, bolingo_sequestrado.mapa_mapeamento, bolingo_sequestrado.pos_robo);
			if(possiveis_posicoes.length!=1){
				Matrizes.imprimeMat(possiveis_posicoes);
				//calcular a que tem maior potencial
				int [] destino = Matrizes.melhorOpcao(bolingo.mapa_mapeamento, bolingo_sequestrado.mapa_mapeamento, bolingo_sequestrado.pos_robo, possiveis_posicoes);
				try{			
					Astar A_Estrela = new Astar(bolingo_sequestrado.mapa_mapeamento);
					Posicao atual = new Posicao(bolingo_sequestrado.pos_robo);
					Posicao objetivo = new Posicao(destino);
					RConsole.print("posicao -> destino:\n[ "+bolingo_sequestrado.pos_robo[0]+" , "+bolingo_sequestrado.pos_robo[1]+" ] ->");
					RConsole.print("[ "+objetivo.x+" , "+objetivo.y+" ]\n");
					ArrayList<Integer[]> caminho = A_Estrela.search(atual, objetivo);
					RConsole.println("Caminho de "+caminho.size()+" cel:");
					for (int kji = 0; kji <caminho.size(); kji++) {
						RConsole.print("[ "+caminho.get(kji)[0]+" , "+caminho.get(kji)[1]+" ] ->");
					}
					RConsole.println("\n");
					for (int kji = 0; kji <caminho.size(); kji++) {
						int orientacao_desejada = 0;
						if((bolingo_sequestrado.pos_robo[0]==caminho.get(kji)[0])&&(caminho.get(kji)[1]>bolingo_sequestrado.pos_robo[1])){
							orientacao_desejada = 0;
						}else if((bolingo_sequestrado.pos_robo[0]==caminho.get(kji)[0])&&(caminho.get(kji)[1]<bolingo_sequestrado.pos_robo[1])){
							orientacao_desejada = 2;
						}else if((bolingo_sequestrado.pos_robo[1]==caminho.get(kji)[1])&&(caminho.get(kji)[0]>bolingo_sequestrado.pos_robo[0])){
							orientacao_desejada = 3;
						}else if((bolingo_sequestrado.pos_robo[1]==caminho.get(kji)[1])&&(caminho.get(kji)[0]<bolingo_sequestrado.pos_robo[0])){
							orientacao_desejada = 1;
						}
						int contador = 0;
						int orientacao_r = bolingo_sequestrado.orientacao_robo;
						while(orientacao_r!=orientacao_desejada){
							orientacao_r++;
							contador++;
							if(orientacao_r>3){
								orientacao_r = 0;
							}
						}
						switch(contador){
						case 3:
							bolingo_sequestrado.giraD();
							Navegation.virar90Direita();
							break;
						case 2:
							bolingo_sequestrado.giraD();
							bolingo_sequestrado.giraD();
							Navegation.virar90Direita();
							Navegation.virar90Direita();
							break;
						case 1:
							bolingo_sequestrado.giraE();
							Navegation.virar90Esquerda();
							break;
						case 0:									
							break;
						}		
						//ler celula
						//Matrizes.ImprimeEstadoRobo(bolingo);


						bolingo_sequestrado.avancaCelula();	
						Navegation.avancarCelula(limite);

						//Matrizes.ImprimeEstadoRobo(bolingo);
						//RConsole.println("[ "+bolingo.pos_robo[0]+" , "+bolingo.pos_robo[1]+" ]");

					}							
				}catch(Exception e){
					e.printStackTrace();
				}
			}else{
				RConsole.println("pos atual [ "+possiveis_posicoes[0][0]+" , "+possiveis_posicoes[0][1]+" ]");
				LCD.drawString("pos[ "+possiveis_posicoes[0][0]+" , "+possiveis_posicoes[0][1]+" ]", 0, 3);
				Button.waitForAnyPress();
				terminar = true;
			}
			posicoes = possiveis_posicoes;
		}while(!terminar);		
		Matrizes.imprimeMat(posicoes);
		RConsole.println("movimentou-se em :"+movimentos+" celulas");

	}	

}



