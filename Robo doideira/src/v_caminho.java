


public class v_caminho{
	int[][] mapa;
	int[] celulaA;
	int[] celulaB;
	
	public v_caminho(int[][] mapa,int[] celulaA, int[] celulaB){
		this.mapa = new int[mapa.length][mapa[0].length];
		for(int i = 0; i<mapa.length; i++){
			for(int j = 0; j<mapa[0].length; j++){
				this.mapa[i][j] = mapa[i][j];
			}
		}
		this.celulaA = celulaA;
		this.celulaB = celulaB;
	}
	public boolean start(){
		boolean saida;		
		saida = buscar(celulaA[0],celulaA[1]);		
		return saida;
	}
	private boolean buscar(int i,int j){
		boolean c1 = false;
		boolean c2 = false;
		boolean c3 = false;
		boolean c4 = false;
		if(i==celulaB[0]&&j==celulaB[1]){//se chegar ao destino parar
			c1 = true;
			c2 = true;
			c3 = true;
			c4 = true;
		}else if(mapa[i][j]!=2){//se a celula atual nao � um obstaculo
			mapa[i][j] = 2;
			if(i+1<mapa.length){
				c1 = buscar(i+1,j);	
			}
			if(i-1>=0){
				c2 = buscar(i-1,j);
			}
			if(j+1<mapa[0].length){
				c3 = buscar(i,j+1);
			}
			if(j-1>=0){
				c4 = buscar(i,j-1);
			}
		}
		return c1||c2||c3||c4;
	}
}

