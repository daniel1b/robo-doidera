
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import lejos.util.Delay;

public class Leitura {

	// SET
	static LightSensor sensor_direita = new LightSensor(SensorPort.S1);
	static LightSensor sensor_esquerda = new LightSensor(SensorPort.S3);
	static UltrasonicSensor sensorDir = new UltrasonicSensor(SensorPort.S4) ;
	static UltrasonicSensor sensorEsq = new UltrasonicSensor(SensorPort.S2) ;

	// M�TODO CALIBRA��O: Retorna m�dia de 10 valores medidos pra cada sensor
	public static int[] Calibracao() {
		int[] soma = { 0, 0 };

		for (int j = 0; j < 10; j++) {
			soma[0] += sensor_esquerda.getLightValue();
			soma[1] += sensor_direita.getLightValue();
			Delay.msDelay(100);

		}
		soma[0] = soma[0] / 10;
		soma[1] = soma[1] / 10;

		LCD.drawString("Esquerdo " + soma[0], 0, 1);
		LCD.drawString("Direito " + soma[1], 0, 3);

		return soma;
	}

	public static int[] LerSensores() {
		int leitura_esquerda = sensor_esquerda.getLightValue();
		int leitura_direita = sensor_direita.getLightValue();

		LCD.drawString("Esquerdo " + leitura_esquerda, 0, 1);
		LCD.drawString("Direito " + leitura_direita, 0, 3);

		int[] vetor = { leitura_esquerda, leitura_direita };
		return vetor;
	}

	public static int[] LerSensoresArray(int[] limiteArray) {

		int leitura_esquerda = sensor_esquerda.getLightValue();
		int leitura_direita = sensor_direita.getLightValue();

		if (leitura_direita <= limiteArray[1]) {
			LCD.drawString("XXX", 0, 1);
		} else {
			LCD.drawString("OOO", 0, 1);
		}
		if (leitura_esquerda <= limiteArray[0]) {
			LCD.drawString("XXX", 12, 1);
		} else {
			LCD.drawString("OOO", 12, 1);
		}

		int[] vetor = { leitura_esquerda, leitura_direita };
		return vetor;
	}
	
	// m�todo para retorna a dist�ncia 
	public static boolean valorSensorUltraSon(){
		//leitura da frente
		boolean l0 = false;
		int leitura0 = 0;;
		for(int i = 0; i < 5; i++){
			leitura0 += sensorDir.getDistance();
			Delay.msDelay(50);
		}
		int media0 = leitura0/5;
		LCD.drawString("  leitura:"+ media0+"  ", 0, 2);
		l0 = media0<=24;
		
		//leitura da direita
		Navegation.virarDireita(10);
		boolean l1 = false;
		int leitura1 = 0;;
		for(int i = 0; i < 5; i++){
			leitura1 += sensorDir.getDistance();
			Delay.msDelay(50);
		}
		int media1 = leitura1/5;
		LCD.drawString("  leitura:"+ media1+"  ", 0, 2);
		l1 = media1<=24;
		Navegation.virarEsquerda(10);

		//leitura da esquerda
		
		Navegation.virarEsquerda(10);
		boolean l2 = false;
		int leitura2 = 0;;
		for(int i = 0; i < 5; i++){
			leitura2 += sensorDir.getDistance();
			Delay.msDelay(50);
		}
		int media2 = leitura2/5;
		LCD.drawString("  leitura:"+ media2+"  ", 0, 2);
		l0 = media2<=24;
		Navegation.virarDireita(10);
		
		return l0||l1||l2;
		
	}
	public static int distanciaUltrasom(){
		//leitura da frente		
		int leitura0 = sensorDir.getDistance();				
		return leitura0;
		
	}
	public static boolean obstaculo_a_frente(int dist){
		int s0 = 0;
		int s1 = 0;
		sensorDir.continuous();
		for(int i = 0; i <5; i++){
			s0 += sensorDir.getDistance();
			Delay.msDelay(50);
		}		
		sensorDir.off();
		sensorEsq.continuous();
		for(int i = 0; i <5; i++){
			s1 += sensorEsq.getDistance();
			Delay.msDelay(50);
		}		
		sensorEsq.off();
		s0 = s0/5;
		s1 = s1/5;
		return (s0<dist)||(s1<dist);
	}
	public static boolean obstaculo(){
		boolean frente = obstaculo_a_frente(23);
		
		Navegation.virarDireita(10);
		boolean direita = obstaculo_a_frente(23);
		Navegation.virarEsquerda(10);
		
		Navegation.virarEsquerda(10);
		boolean esquerda = obstaculo_a_frente(23);
		Navegation.virarDireita(10);
		
		return frente||direita||esquerda;
	}
}

