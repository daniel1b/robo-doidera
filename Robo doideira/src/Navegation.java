
import java.util.Random;

import lejos.nxt.LCD;
import lejos.nxt.Motor;

public class Navegation {
	
    static int velocidade = 150; 
	static float tamanhoRoda = (float)(5.3/2.0);
	static float tamanhoCelula = 20;
	static float eixo = (float)11.5;
	static int valorSensor;
    static int[] guardaValorSensor;
    static Random gerador = new Random();
	
	
	
	public static void andarParaFrente(){
		
		if (gerador.nextBoolean())
			
		{
			Motor.A.setSpeed(velocidade);
			Motor.A.forward();
			Motor.B.setSpeed(velocidade);
			Motor.B.forward();	
		}
		else
		{
			Motor.B.setSpeed(velocidade);
			Motor.B.forward();
			Motor.A.setSpeed(velocidade);
			Motor.A.forward();
		}
			

		
	}
	
	public static void andarParaTraz(){
		Motor.A.setSpeed(velocidade);
		Motor.A.backward();
		Motor.B.setSpeed(velocidade);
		Motor.B.backward();
	}
	
	public static void avancarCelula(int[] limite){
		//

		andarParaFrente();
		//LCD.clear();
		guardaValorSensor = Leitura.LerSensoresArray(limite);
		LCD.clear();
		LCD.drawString("buscar linha", 0, 3);
		while( guardaValorSensor[0] > limite[0] &&
				guardaValorSensor[1] > limite[1] )
		{
			guardaValorSensor = Leitura.LerSensoresArray(limite);
		}
		//parar();
		
		if ( guardaValorSensor[0] <= limite[0] )
		{
			Motor.A.setSpeed((int)(velocidade/3.0));
			Motor.B.setSpeed(0);
			Motor.A.forward();
		}else
		{
			Motor.A.setSpeed(0);
			Motor.B.setSpeed((int)(velocidade/3.0));
			Motor.B.forward();
		}
		//LCD.clear();
		LCD.drawString("ajeita ang", 3, 3);
		while( guardaValorSensor[0] > limite[0] ||
				guardaValorSensor[1] > limite[1] )
		{
			guardaValorSensor = Leitura.LerSensoresArray(limite);
		}
				
		//parar();
		LCD.clear();
		LCD.drawString("chegar cen", 3, 3);
		Motor.A.resetTachoCount();
		Motor.B.resetTachoCount();
		andarParaFrente();
		while((( Motor.A.getTachoCount() * 
				tamanhoRoda * 3.14/180.0)+(Motor.B.getTachoCount() * 
						tamanhoRoda* 3.14/180.0))/2.0 <= (tamanhoCelula/2.0)+6.5) 
		{
			LCD.drawString(""+Motor.A.getTachoCount(), 0, 1);
			LCD.drawString(""+Motor.B.getTachoCount(), 12, 1);
		}
		parar();
		
	}
	
	public static void virar90Esquerda(){
		Motor.A.resetTachoCount();
		Motor.B.resetTachoCount();
		Motor.B.setSpeed((int)(velocidade/2.0));
		Motor.B.forward();
		Motor.A.setSpeed((int)(velocidade/2.0));
		Motor.A.backward();		
		LCD.clear();
		LCD.drawString("<<----", 3, 3);
		float angulo_desej = (float)((86.0)*((float)eixo/2.0)*(1.0/(float)tamanhoRoda));
		while(Math.abs(Motor.A.getTachoCount()-Motor.B.getTachoCount())/2.0<angulo_desej) {
			LCD.drawString(""+Motor.A.getTachoCount(), 0, 1);
			LCD.drawString(""+Motor.B.getTachoCount(), 12, 1);
		}
		parar();
		
	}
	
	public static void virarEsquerda(int angulo){
		Motor.A.resetTachoCount();
		Motor.B.resetTachoCount();
		Motor.B.setSpeed((int)(velocidade/2.0));
		Motor.B.forward();
		Motor.A.setSpeed((int)(velocidade/2.0));
		Motor.A.backward();		
		LCD.clear();
		LCD.drawString("<<----", 3, 3);
		float angulo_desej = (float)(((float)angulo)*((float)eixo/2.0)*(1.0/(float)tamanhoRoda));
		while(Math.abs(Motor.A.getTachoCount()-Motor.B.getTachoCount())/2.0<angulo_desej) {
			LCD.drawString(""+Motor.A.getTachoCount(), 0, 1);
			LCD.drawString(""+Motor.B.getTachoCount(), 12, 1);
		}
		parar();
		
	}
	
	public static void virar90Direita(){
		Motor.A.resetTachoCount();
		Motor.B.resetTachoCount();
		Motor.B.setSpeed((int)(velocidade/2.0));
		Motor.B.backward();
		Motor.A.setSpeed((int)(velocidade/2.0));
		Motor.A.forward();
		LCD.clear();
		LCD.drawString("---->>", 3, 3);
		float angulo_desej = (float)((86.0)*((float)eixo/2.0)*(1.0/(float)tamanhoRoda));
		while(Math.abs(Motor.A.getTachoCount()-Motor.B.getTachoCount())/2.0<angulo_desej) {
			LCD.drawString(""+Motor.A.getTachoCount(), 0, 1);
			LCD.drawString(""+Motor.B.getTachoCount(), 12, 1);
		}
		parar();
		
	}
	
	public static void virarDireita(int angulo){
		Motor.A.resetTachoCount();
		Motor.B.resetTachoCount();
		Motor.B.setSpeed((int)(velocidade/2.0));
		Motor.B.backward();
		Motor.A.setSpeed((int)(velocidade/2.0));
		Motor.A.forward();
		LCD.clear();
		LCD.drawString("---->>", 3, 3);
		float angulo_desej = (float)(((float)angulo)*((float)eixo/2.0)*(1.0/(float)tamanhoRoda));
		while(Math.abs(Motor.A.getTachoCount()-Motor.B.getTachoCount())/2.0<angulo_desej) {
			LCD.drawString(""+Motor.A.getTachoCount(), 0, 1);
			LCD.drawString(""+Motor.B.getTachoCount(), 12, 1);
		}
		parar();
		
	}
	
	// parar
	public static void parar(){
		Motor.A.stop(true);
		Motor.B.stop();
	}
	public static void centralizarNaCelula(int[] limite){
		virarDireita(86);
		boolean parede = avancarlinha(limite,tamanhoCelula);
		//dar meia volta
		if(parede){
			retroceder_cm((float)(tamanhoCelula/4));
		}
		else{
			retroceder_cm((float)(tamanhoCelula/8));
		}
		virarEsquerda(86);
	}
	public static void centralizarNaCelula2(int[] limite){
		boolean parede = avancarlinha(limite,(float)(tamanhoCelula/1.5));
		//dar meia volta
		if(parede){
			retroceder_cm((float)(tamanhoCelula/4));
		}
		else{
			retroceder_cm((float)(tamanhoCelula/8));
		}
	}
	public static boolean varrerCelula(int[] limite){
		///distanciaUltrasom
		boolean leitura0 = false, leitura1 = false, leitura2 = false;
		leitura0 = Leitura.valorSensorUltraSon();
		if(!leitura0){
			//dislocar-se para direita
			avancarlinha(limite,tamanhoCelula);
			boolean parede = avancarlinha(limite,tamanhoCelula);
			//dar meia volta
			if(parede){
				retroceder_cm((float)(tamanhoCelula/3.5));
			}
			else{
				retroceder_cm((float)(tamanhoCelula/4.5));				
			}
			virarDireita(86);
			parede = avancarlinha(limite,tamanhoCelula);
			//dar meia volta
			if(parede){
				retroceder_cm((float)(tamanhoCelula/8.0));
			}
			else{
				retroceder_cm((float)(tamanhoCelula/11.0));
			}
			virarEsquerda(86);		
			leitura1 = Leitura.valorSensorUltraSon();
			if(!leitura1){
				virarEsquerda(86);	
				parede = avancarlinha(limite,tamanhoCelula);
				//dar meia volta
				if(parede){
					retroceder_cm((float)(tamanhoCelula/8.0));
				}
				else{
					retroceder_cm((float)(tamanhoCelula/11.0));
				}
				//dar meia volta
				virarDireita(86);		
				leitura2 = Leitura.valorSensorUltraSon();
				virarDireita(86);
				//avancar tamanho da celula/4
				avanca_cm((float)(tamanhoCelula/5.0));
				virarEsquerda(86);
			}else{
				virarEsquerda(86);	
				//avancar tamanho da celula/4
				avanca_cm((float)(tamanhoCelula/5.0));
				virarDireita(86);
			}
		}
		return leitura0||leitura1||leitura2;		
	}
	public static boolean avancarlinha(int[] limite, float max){
		boolean saida = true;
		Motor.A.resetTachoCount();
		Motor.B.resetTachoCount();
		Motor.A.setSpeed(velocidade/2);
		Motor.A.forward();
		Motor.B.setSpeed(velocidade/2);
		Motor.B.forward();
		//LCD.clear();
		guardaValorSensor = Leitura.LerSensoresArray(limite);
		LCD.clear();
		LCD.drawString("buscar linha", 0, 3);
		while( guardaValorSensor[0] > limite[0] &&
				guardaValorSensor[1] > limite[1] && (( Motor.A.getTachoCount() * 
						tamanhoRoda * 3.14/180.0)+(Motor.B.getTachoCount() * 
								tamanhoRoda* 3.14/180.0))/2.0 <= max){
			guardaValorSensor = Leitura.LerSensoresArray(limite);
		}
		if ( guardaValorSensor[0] <= limite[0] ){
			Motor.A.setSpeed((int)(velocidade/2.0));
			Motor.B.setSpeed(0);
			Motor.A.forward();
		}else{
			Motor.A.setSpeed(0);
			Motor.B.setSpeed((int)(velocidade/2.0));
			Motor.B.forward();
		}
		
		if((( Motor.A.getTachoCount() * 
				tamanhoRoda * 3.14/180.0)+(Motor.B.getTachoCount() * 
						tamanhoRoda* 3.14/180.0))/2.0 < max){
			while( guardaValorSensor[0] > limite[0] ||
					guardaValorSensor[1] > limite[1] ){
				LCD.drawString("ajeita ang       ", 3, 3);
				guardaValorSensor = Leitura.LerSensoresArray(limite);
			}
		}else{
			saida = false;
		}	
		parar();
		return saida;
	}
	public static void avanca_cm (float cm){
		LCD.drawString("andar: "+cm+" cm    ", 3, 3);
		Motor.A.resetTachoCount();
		Motor.B.resetTachoCount();
		Motor.A.setSpeed(velocidade/2);
		Motor.A.forward();
		Motor.B.setSpeed(velocidade/2);
		Motor.B.forward();
		while((( Motor.A.getTachoCount() * 
				tamanhoRoda * 3.14/180.0)+(Motor.B.getTachoCount() * 
						tamanhoRoda* 3.14/180.0))/2.0 <= cm) 
		{
			LCD.drawString(""+Motor.A.getTachoCount(), 0, 1);
			LCD.drawString(""+Motor.B.getTachoCount(), 12, 1);
		}
		parar();
	}
	public static void retroceder_cm (float cm){
		LCD.drawString("andar: "+cm+" cm    ", 3, 3);
		Motor.A.resetTachoCount();
		Motor.B.resetTachoCount();
		Motor.B.setSpeed(velocidade/2);
		Motor.B.backward();
		Motor.A.setSpeed(velocidade/2);
		Motor.A.backward();
		while(Math.abs(( Motor.A.getTachoCount() * 
				tamanhoRoda * 3.14/180.0)+(Motor.B.getTachoCount() * 
						tamanhoRoda* 3.14/180.0))/2.0 <= cm) 
		{
			LCD.drawString(""+Motor.A.getTachoCount(), 0, 1);
			LCD.drawString(""+Motor.B.getTachoCount(), 12, 1);
		}
		parar();
	}
	
}




